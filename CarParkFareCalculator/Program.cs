﻿using FareCalculator.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarParkFareCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestMethod();

            try
            {
                // Check for input arguments count
                if (args.Count() != 2)
                {
                    InputDateTimeFormats();
                    throw new InvalidOperationException("Invalid no of Arguments Supplied to the app. Please try format `CarParkfareCalculator.exe StartDateTime EndDateTime` using the examples above.");
                }

                // Calculate fare
                var rateCalc = new CarParkRateEngine();
                var ret = rateCalc.CalculateRateNameWithFare(args[0], args[1]);

                Console.WriteLine($"Response - The Rate Name: {ret.Item1}, Total Fare: ${ret.Item2}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Something went wrong - Exiting Rate Calculator program - {ex.Message}");
            }
        }

        private static void InputDateTimeFormats()
        {
            Console.WriteLine("Invalid Format - Some Examples of supported formats for start/End dates are:");
            Console.WriteLine("Format - dd/mm/yyyy h24:mi:ss, sample - 23/03/2017 14:20:33");
            Console.WriteLine("Format - yyyy-mm-dd h24:mi:ss, sample 2018-08-22 16:25:31");
            Console.WriteLine("Format - dd Mon yyyy hh:mi:ss.ms AM/PM, sample - 12 Feb 2018 11:55:38.30 AM \n");
        }

        private static void TestMethod()
        {
            string[] dateStrings = {"18/12/2009 14:57:32.8", "2009-05-25 14:57:32.8",
                              "2009-05-22T14:57:32.8375298-04:00", "5/01/2008",
                              "30/01/2008 14:57:32.80 -07:00",
                              "1 May 2008 2:57:32.8 PM", "16-05-2009 1:00:32 PM",
                              "Fri, 15 May 2009 20:10:57 GMT" };
            DateTime dateValue;

            Console.WriteLine("Attempting to parse strings using {0} culture.",
                              CultureInfo.CurrentCulture.Name);
            foreach (string dateString in dateStrings)
            {
                if (DateTime.TryParse(dateString, out dateValue))
                    Console.WriteLine("  Converted '{0}' to {1} ({2}).", dateString,
                                      dateValue, dateValue.Kind);
                else
                    Console.WriteLine("  Unable to parse '{0}'.", dateString);
            }
        }
    }
}
