﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FareCalculator.Core
{
    public sealed class CarParkRateEngine
    {
        private DateTime _startDatetime;
        private DateTime _endDatetime;

        public void ValidateInput(string startTime, string endTime)
        {
            if (!DateTime.TryParse(startTime, out _startDatetime))
            {
                InputDateTimeFormats();
                throw new Exception($"Invalid Start Date Time format: {startTime}");
            }

            if (!DateTime.TryParse(endTime, out _endDatetime))
            {
                InputDateTimeFormats();
                throw new Exception($"Invalid End Date Time format: {endTime}");
            }

            // Check End Date less than Start Date
            if (_endDatetime < _startDatetime)
                throw new Exception("End DateTime should be greater than Start DateTime");

            Console.WriteLine($"StartTime : {_startDatetime}, EndTime: {_endDatetime}");
        }

        private void InputDateTimeFormats()
        {
            Console.WriteLine("Invalid Format - Some Examples of supported formats for start/End dates are:");
            Console.WriteLine("Format - dd/mm/yyyy h24:mi:ss, sample - 23/03/2017 14:20:33");
            Console.WriteLine("Format - yyyy-mm-dd h24:mi:ss, sample 2018-08-22 16:25:31");
            Console.WriteLine("Format - dd Mon yyyy hh:mi:ss.ms AM/PM, sample - 12 Feb 2018 11:55:38.30 AM \n");
        }

        public Tuple<string, Double> CalculateRateNameWithFare(string startTime, string endTime)
        {
            try
            {
                // Check input time's format
                ValidateInput(startTime, endTime);

                // Get the Day's of the week
                var startDayOfWeek = _startDatetime.DayOfWeek;
                var endDayOfWeek = _endDatetime.DayOfWeek;

                // Times's of the day
                var strtTime = _startDatetime.TimeOfDay;
                var endxTime = _endDatetime.TimeOfDay;

                // Difference between the dates (EB - 0 and NR - 1)
                var dateDiff = _endDatetime.DayOfYear - _startDatetime.DayOfYear;

                // Determine Week Day
                if (startDayOfWeek >= DayOfWeek.Monday && startDayOfWeek <= DayOfWeek.Friday
                    && endDayOfWeek >= DayOfWeek.Monday && startDayOfWeek <= DayOfWeek.Saturday
                    // For Early Bird the start and End date are on the same day of the Year (so difference is '0')
                    // For Night Rate the End date and start date are separated by zero or one day difference (so difference are '0' and '1')
                    && dateDiff <=1)
                {
                    // Early bird time frames
                    var EBStartTime = strtTime >= new TimeSpan(6, 0, 0) && strtTime <= new TimeSpan(9, 0, 0);
                    var EBEndTime = endxTime >= new TimeSpan(15, 30, 0) && endxTime <= new TimeSpan(23, 30, 0);

                    // Night Rate time frames
                    var NRStartTime = (strtTime >= new TimeSpan(18, 0, 0) && strtTime <= new TimeSpan(23, 59, 59)) 
                                     || (strtTime == new TimeSpan(00, 00, 00));
                    var NREndTime = (endxTime >= new TimeSpan(18, 0, 0) && endxTime <= new TimeSpan(23, 59, 59))
                                    || (endxTime >= new TimeSpan(0, 0, 0) && endxTime <= new TimeSpan(5, 59, 59));

                    // Check for Early bird
                    if (dateDiff == 0 && EBStartTime && EBEndTime)
                    {
                        // Early Bird Rate Name and its flat rate of $13.00.
                        return new Tuple<string, double>("Early Bird", 13.00);
                    }
                    // Late Night can enter and exit on same day as well
                    else if (dateDiff <= 1 && NRStartTime && NREndTime)
                    {
                        // Night Rate Name and its flat rate of $6.50.
                        return new Tuple<string, double>("Night Rate", 6.50);
                    }
                    // As outside the EB and NR, apply Standard Rates
                    // based on the no. of days OR hour difference if on the same day.
                    else
                    {
                        return ComputeStandardRate(dateDiff);
                    }
                }
                // Determine Weekend
                else if ((startDayOfWeek == DayOfWeek.Saturday || startDayOfWeek == DayOfWeek.Sunday)
                    && (endDayOfWeek == DayOfWeek.Saturday || endDayOfWeek == DayOfWeek.Sunday))
                {
                    // Weekend Rate Name and its flat rate of $10.00.
                    return new Tuple<string, double>("Weekend Rate", 10.00);
                }
                // Apply Standard Rate
                else
                {
                    return ComputeStandardRate(dateDiff);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Console.WriteLine($"StackTrace - {ex.StackTrace}");
#endif
                Console.WriteLine($"Error in Rate Calc Engine: {ex.Message}");
                
                // Something has gone wrong, so no rate apllied with the error message pushed to the user.
                return new Tuple<string, double>(ex.Message, 0.0);
            }
        }

        private Tuple<string, double> ComputeStandardRate(int dateDiff)
        {
            double rate;

            if (dateDiff >= 1)
            {
                // $20 for each calendar Day
                rate = 20 * (dateDiff + 1);
            }
            else
            {
                var timeDiff = _endDatetime.Subtract(_startDatetime);
                if (timeDiff <= new TimeSpan(1, 0, 0))
                    rate = 5;
                else if (timeDiff > new TimeSpan(1, 0, 0) && timeDiff <= new TimeSpan(2, 0, 0))
                    rate = 10;
                else if (timeDiff > new TimeSpan(2, 0, 0) && timeDiff <= new TimeSpan(3, 0, 0))
                    rate = 15;
                else
                    rate = 20;
            }

            return new Tuple<string, double>("Standard Rate", rate);
        }
    }
}
