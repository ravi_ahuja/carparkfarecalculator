﻿using System;
using FareCalculator.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarParkFareCalcTests
{
    [TestClass]
    public class CarParkRateCalcTests
    {
        [TestMethod()]
        public void EndDateLessThanStartDate()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("16/03/2018 06:57:32", "16/03/2018 05:43:32");

            // Validate Results
            Assert.AreEqual(result.Item1, "End DateTime should be greater than Start DateTime");
            Assert.AreEqual(result.Item2, 0.0);
        }

        [TestMethod()]
        public void EarlyBirdTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("16/03/2018 06:57:32", "16/03/2018 15:43:32");

            // Validate Results
            Assert.AreEqual(result.Item1, "Early Bird");
            Assert.AreEqual(result.Item2, 13.00);
        }

        // ******** Early Bird Boundarty tests ***********
        [TestMethod()]
        public void EarlyBird_Start_EndDate_BeginBoundaryTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("16/03/2018 06:00:00", "16/03/2018 15:30:00");

            // Validate Results
            Assert.AreEqual(result.Item1, "Early Bird");
            Assert.AreEqual(result.Item2, 13.00);
        }

        [TestMethod()]
        public void EarlyBird_Start_EndDate_EndBoundaryTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("16/03/2018 09:00:00", "16/03/2018 23:30:00");

            // Validate Results
            Assert.AreEqual(result.Item1, "Early Bird");
            Assert.AreEqual(result.Item2, 13.00);
        }

        [TestMethod()]
        public void EarlyBird_StartDate_Outside_EdgeCaseTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("16/03/2018 05:59:59", "16/03/2018 23:30:00");

            // Validate Results
            Assert.AreEqual(result.Item1, "Standard Rate");
            Assert.AreEqual(result.Item2, 20.00);
        }
        // ******** Early Bird Boundarty tests ***********

        [TestMethod()]
        public void NightRateTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("18/12/2017 18:57:32", "19/12/2017 5:43:32");

            // Validate Results
            Assert.AreEqual(result.Item1, "Night Rate");
            Assert.AreEqual(result.Item2, 6.50);
        }

        // ******** Night Rate Boundarty tests ***********
        [TestMethod()]
        public void NightRate_EdgeCaseTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("18/12/2017 18:00:00", "19/12/2017 5:59:59");

            // Validate Results
            Assert.AreEqual(result.Item1, "Night Rate");
            Assert.AreEqual(result.Item2, 6.50);
        }

        [TestMethod()]
        public void NightRate_LateNightEntry_Test()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("18/12/2017 00:00:00", "19/12/2017 5:59:59");

            // Validate Results
            Assert.AreEqual(result.Item1, "Night Rate");
            Assert.AreEqual(result.Item2, 6.50);
        }

        [TestMethod()]
        public void NightRate_StartTimeOutside_StdApplyTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("18/12/2017 17:59:59", "19/12/2017 5:59:59");

            // Validate Results
            Assert.AreEqual(result.Item1, "Standard Rate");
            Assert.AreEqual(result.Item2, 40.00);
        }

        [TestMethod()]
        public void NightRate_EndTimeOutside_StdApplyTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("18/12/2017 18:00:00", "19/12/2017 6:00:00");

            // Validate Results
            Assert.AreEqual(result.Item1, "Standard Rate");
            Assert.AreEqual(result.Item2, 40.00);
        }

        [TestMethod()]
        public void NightRate_Friday_Sat_EarlyExit_SpeicalCase_Test()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("16/03/2018 18:00:00", "17/03/2018 5:59:59");

            // Validate Results
            Assert.AreEqual(result.Item1, "Night Rate");
            Assert.AreEqual(result.Item2, 6.50);
        }
        // ******** Night Rate Boundarty tests ***********

        // ******** Weekend Rate tests ***********
        [TestMethod()]
        public void WeekendRate_SatSun_Test()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("17/03/2018 00:00:01", "18/03/2018 23:59:59");

            // Validate Results
            Assert.AreEqual(result.Item1, "Weekend Rate");
            Assert.AreEqual(result.Item2, 10.00);
        }

        [TestMethod()]
        public void WeekendRate_SameDayExitTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("17/03/2018 19:00:01", "17/03/2018 23:59:59");

            // Validate Results
            Assert.AreEqual(result.Item1, "Weekend Rate");
            Assert.AreEqual(result.Item2, 10.00);
        }

        [TestMethod()]
        public void WeekendRate_BoundaryTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("17/03/2018 00:00:01", "18/03/2018 23:59:59");

            // Validate Results
            Assert.AreEqual(result.Item1, "Weekend Rate");
            Assert.AreEqual(result.Item2, 10.00);
        }

        [TestMethod()]
        public void WeekendRate_EndateOutside_BoundaryTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("17/03/2018 00:00:01", "19/03/2018 00:00:01");

            // Validate Results
            Assert.AreEqual(result.Item1, "Standard Rate");
            Assert.AreEqual(result.Item2, 60.00);
        }

        [TestMethod()]
        public void StandardRate_LongWeekday_Test()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("12/03/2018 11:00:00", "14/03/2018 15:30:00");

            // Validate Results
            Assert.AreEqual(result.Item1, "Standard Rate");
            Assert.AreEqual(result.Item2, 60.00);
        }

        [TestMethod()]
        public void StandardRate_EndDate_OutsideWindowTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("17/03/2018 00:00:01", "19/03/2018 00:00:01");

            // Validate Results
            Assert.AreEqual(result.Item1, "Standard Rate");
            Assert.AreEqual(result.Item2, 60.00);
        }

        [TestMethod()]
        public void StandardRate_EndDateOutsideWeekend_EdgeCaseTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("16/03/2018 00:00:01", "17/03/2018 06:00:00");

            // Validate Results
            Assert.AreEqual(result.Item1, "Standard Rate");
            Assert.AreEqual(result.Item2, 40.00);
        }

        [TestMethod()]
        public void WeekendRate_FriDayNight_SatEarly_Outside_EdgeCaseTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("16/03/2018 00:00:01", "17/03/2018 06:00:00");

            // Validate Results
            Assert.AreEqual(result.Item1, "Standard Rate");
            Assert.AreEqual(result.Item2, 40.00);
        }

        [TestMethod()]
        public void StandardRate_LongWeekend_FriToMon_EdgeCaseTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("16/03/2018 21:00:01", "19/03/2018 09:25:00");

            // Validate Results
            Assert.AreEqual(result.Item1, "Standard Rate");
            Assert.AreEqual(result.Item2, 80.00);
        }

        [TestMethod()]
        public void StandardRate_WithStart_OveralapWithEarlyBird_1hrTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("19/03/2018 08:45:00", "19/03/2018 09:45:00");

            // Validate Results
            Assert.AreEqual(result.Item1, "Standard Rate");
            Assert.AreEqual(result.Item2, 5.00);
        }

        [TestMethod()]
        public void StandardRate_WithStart_OveralapWithEarlyBird_justover1hrTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("19/03/2018 08:45:00", "19/03/2018 09:45:01");

            // Validate Results
            Assert.AreEqual(result.Item1, "Standard Rate");
            Assert.AreEqual(result.Item2, 10.00);
        }

        [TestMethod()]
        public void StandardRate_WithStart_OveralapWithEarlyBird_3plushrTest()
        {
            // Instantiate
            var client = new CarParkRateEngine();

            // Execute
            var result = client.CalculateRateNameWithFare("19/03/2018 08:45:01", "19/03/2018 11:46:00");

            // Validate Results
            Assert.AreEqual(result.Item1, "Standard Rate");
            Assert.AreEqual(result.Item2, 20.00);
        }
    }
}
